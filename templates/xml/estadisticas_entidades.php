<?xml version="1.0" encoding="UTF-8" ?>
<estadisticas> 
<?php foreach ($estadisticas as $entidad) { ?>
	<entidad>
		<clave_entidad><?php echo $entidad['clave_entidad']; ?> </clave_entidad>
		<entidad><?php echo $entidad['entidad'] ;?></entidad>
		<municipios><?php echo $entidad['municipios'] ;?></municipios>
		<localidades><?php echo $entidad['localidades'] ;?></localidades>
		<razones_sociales><?php echo $entidad['razones_sociales'] ;?></razones_sociales>
	</entidad>
<?php } ?>
</estadisticas>