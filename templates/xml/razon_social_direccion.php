<?xml version="1.0" encoding="UTF-8"?>
<razones_sociales> 
<?php  foreach ($razones_sociales as $razon_social){ ?>
	<razon_social>
	        <nombre> <?php echo $razon_social['nombre']; ?></nombre>
	        <tipo_viabilidad><?php echo $razon_social['tipo_viabilidad']; ?></tipo_viabilidad>
	        <nombre_viabilidad> <?php echo $razon_social['nombre_viabilidad']; ?></nombre_viabilidad>
	        <tipo_viabilidad1><?php echo $razon_social['tipo_viabilidad1']; ?></tipo_viabilidad1>
	        <nombre_viabilidad1><?php echo $razon_social['nombre_viabilidad1']; ?></nombre_viabilidad1>
	        <tipo_viabilidad2><?php echo $razon_social['tipo_viabilidad2']; ?></tipo_viabilidad2>
	        <nombre_viabilidad2> <?php echo $razon_social['nombre_viabilidad2']; ?></nombre_viabilidad2>
	        <tipo_viabilidad3><?php echo $razon_social['tipo_viabilidad3']; ?></tipo_viabilidad3>
	        <nombre_viabilidad3><?php echo $razon_social['nombre_viabilidad3']; ?></nombre_viabilidad3>
	        <numero_exterior><?php echo $razon_social['numero_exterior']; ?></numero_exterior>
	        <letra_exterior><?php echo $razon_social['letra_exterior']; ?></letra_exterior>
	        <edificio><?php echo $razon_social['edificio']; ?></edificio>
	        <edificio_piso>"<?php echo $razon_social['edificio_piso']; ?></edificio_piso>
	        <numero_interior> <?php echo $razon_social['numero_interior']; ?></numero_interior>
	        <letra_interior>"<?php echo $razon_social['letra_interior']; ?></letra_interior>
	        <latitud><?php echo $razon_social['latitud']; ?></latitud>
	        <longitud><?php echo $razon_social['longitud']; ?></longitud>
	        <localidad><?php echo $razon_social['localidad']; ?></localidad>
	        <municipio><?php echo $razon_social['municipio']; ?></municipio>
	        <entidad><?php echo $razon_social['entidad']; ?></entidad>
	</razon_social>
<?php } ?>
</razones_sociales>
