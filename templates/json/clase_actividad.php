{
  "clases_actividades": {
    "clase_actividad": [
<?php
  $total = count($clase_actividad);
  $ultimo = $total - 1;
  $contador = 0;
  foreach ($clase_actividad as $actividad) {
?>
      {
        "codigo_de_actividad": <?php echo $actividad['codigo_de_actividad']; ?>,
        "nombre_de_actividad": "<?php echo $actividad['nombre_de_actividad']; ?>"
<?php
if ($contador !== $ultimo) {
  $fin = '},';
  $contador += 1;
} else {
  $fin = '}';
}
?>
      <?php echo $fin; ?>

<?php } ?>
    ]
  }
}
