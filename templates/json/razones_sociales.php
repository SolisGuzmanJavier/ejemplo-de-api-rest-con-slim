{
  "razones_sociales": {
    "razon_social": [
<?php
  $total = count($razones_sociales);
  $ultimo = $total - 1;
  $contador = 0;
  foreach ($razones_sociales as $razon_social) {
?>
      {
        "id": "<?php echo $razon_social['id']; ?>",
        "nombre": "<?php echo $razon_social['nombre']; ?>",
        "razon_social": "<?php echo $razon_social['razon_social']; ?>",
        "codigo_de_actividad": "<?php echo $razon_social['codigo_de_actividad']; ?>",
        "nombre_de_actividad": "<?php echo $razon_social['nombre_de_actividad']; ?>",
        "descripcion_estrato_personal": "<?php echo $razon_social['descripcion_estrato_personal']; ?>",
        "tipo_viabilidad": "<?php echo $razon_social['tipo_viabilidad']; ?>",
        "nombre_viabilidad": "<?php echo $razon_social['nombre_viabilidad']; ?>",
        "tipo_viabilidad1": "<?php echo $razon_social['tipo_viabilidad1']; ?>",
        "nombre_viabilidad1": "<?php echo $razon_social['nombre_viabilidad1']; ?>",
        "tipo_viabilidad2": "<?php echo $razon_social['tipo_viabilidad2']; ?>",
        "nombre_viabilidad2": "<?php echo $razon_social['nombre_viabilidad2']; ?>",
        "tipo_viabilidad3": "<?php echo $razon_social['tipo_viabilidad3']; ?>",
        "nombre_viabilidad3": "<?php echo $razon_social['nombre_viabilidad3']; ?>",
        "numero_exterior": "<?php echo $razon_social['numero_exterior']; ?>",
        "letra_exterior": "<?php echo $razon_social['letra_exterior']; ?>",
        "edificio": "<?php echo $razon_social['edificio']; ?>",
        "edificio_piso": "<?php echo $razon_social['edificio_piso']; ?>",
        "numero_interior": "<?php echo $razon_social['numero_interior']; ?>",
        "letra_interior": "<?php echo $razon_social['letra_interior']; ?>",
        "tipo_asentamiento_humano": "<?php echo $razon_social['tipo_asentamiento_humano']; ?>",
        "nombre_asentamiento_humano": "<?php echo $razon_social['nombre_asentamiento_humano']; ?>",
        "tipo_centro_comercial": "<?php echo $razon_social['tipo_centro_comercial']; ?>",
        "corredor_industrial_centro_comercial": "<?php echo $razon_social['corredor_industrial_centro_comercial']; ?>",
        "numero_local": "<?php echo $razon_social['numero_local']; ?>",
        "codigo_postal": "<?php echo $razon_social['codigo_postal']; ?>",
        "area_geoestadistica_basica": "<?php echo $razon_social['area_geoestadistica_basica']; ?>",
        "manzana": "<?php echo $razon_social['manzana']; ?>",
        "numero_telefono": "<?php echo $razon_social['numero_telefono']; ?>",
        "correo_electronico": "<?php echo $razon_social['correo_electronico']; ?>",
        "sitio_internet": "<?php echo $razon_social['sitio_internet']; ?>",
        "tipo_establecimiento": "<?php echo $razon_social['tipo_establecimiento']; ?>",
        "latitud": "<?php echo $razon_social['latitud']; ?>",
        "longitud":" <?php echo $razon_social['longitud']; ?>",
        "fecha_incorporacion": "<?php echo $razon_social['fecha_incorporacion']; ?>"
<?php
if ($contador !== $ultimo) {
  $fin = '},';
  $contador += 1;
} else {
  $fin = '}';
}
?>
      <?php echo $fin; ?>

<?php } ?>
    ]
  }
}
