{
  "entidades": {
    "entidad": [
<?php
  $total = count($entidades);
  $ultimo = $total - 1;
  $contador = 0;
  foreach ($entidades as $entidad) {
?>
      {
        "clave_entidad": <?php echo $entidad['clave_entidad']; ?>,
        "entidad": "<?php echo $entidad['entidad']; ?>"
<?php
if ($contador !== $ultimo) {
  $fin = '},';
  $contador += 1;
} else {
  $fin = '}';
}
?>
      <?php echo $fin; ?>

<?php } ?>
    ]
  }
}
