{
  "razones_sociales": {
    "razon_social": [
<?php
  $total = count($razones_sociales);
  $ultimo = $total - 1;
  $contador = 0;
  foreach ($razones_sociales as $razon_social) {
?>
      {
        "nombre": "<?php echo $razon_social['nombre']; ?>",
        "numero_telefono": "<?php echo $razon_social['numero_telefono']; ?>",
        "correo_electronico": "<?php echo $razon_social['correo_electronico']; ?>",
        "sitio_internet": "<?php echo $razon_social['sitio_internet']; ?>"        
<?php
if ($contador !== $ultimo) {
  $fin = '},';
  $contador += 1;
} else {
  $fin = '}';
}
?>
      <?php echo $fin; ?>

<?php } ?>
    ]
  }
}
