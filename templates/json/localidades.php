{
  "localidades": {
    "localidad": [
<?php
  $total = count($localidades);
  $ultimo = $total - 1;
  $contador = 0;
  foreach ($localidades as $localidad) {
?>
      {
        "clave_localidad": <?php echo $localidad['clave_localidad']; ?>,
        "nombre": "<?php echo $localidad['localidad']; ?>"
<?php
if ($contador !== $ultimo) {
  $fin = '},';
  $contador += 1;
} else {
  $fin = '}';
}
?>
      <?php echo $fin; ?>

<?php } ?>
    ]
  }
}
