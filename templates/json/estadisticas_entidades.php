{
  "estadisticas": {
    "entidad": [
<?php
  $total = count($estadisticas);
  $ultimo = $total - 1;
  $contador = 0;
  foreach ($estadisticas as $entidad) {
?>
      {
        "clave_entidad": <?php echo $entidad['clave_entidad']; ?>,
        "entidad": "<?php echo $entidad['entidad']; ?>",
        "municipios": "<?php echo $entidad['municipios']; ?>",
        "localidades": "<?php echo $entidad['localidades']; ?>",
        "razones_sociales": "<?php echo $entidad['razones_sociales']; ?>"
<?php
if ($contador !== $ultimo) {
  $fin = '},';
  $contador += 1;
} else {
  $fin = '}';
}
?>
      <?php echo $fin; ?>

<?php } ?>
    ]
  }
}
