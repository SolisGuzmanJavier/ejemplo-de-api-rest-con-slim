{
  "razones_sociales": {
    "razon_social": [
<?php
  $total = count($razones_sociales);
  $ultimo = $total - 1;
  $contador = 0;
  foreach ($razones_sociales as $razon_social) {
?>
      {
        "nombre": "<?php echo $razon_social['nombre']; ?>",
        "tipo_viabilidad": "<?php echo $razon_social['tipo_viabilidad']; ?>",
        "nombre_viabilidad": "<?php echo $razon_social['nombre_viabilidad']; ?>",
        "tipo_viabilidad1": "<?php echo $razon_social['tipo_viabilidad1']; ?>",
        "nombre_viabilidad1": "<?php echo $razon_social['nombre_viabilidad1']; ?>",
        "tipo_viabilidad2": "<?php echo $razon_social['tipo_viabilidad2']; ?>",
        "nombre_viabilidad2": "<?php echo $razon_social['nombre_viabilidad2']; ?>",
        "tipo_viabilidad3": "<?php echo $razon_social['tipo_viabilidad3']; ?>",
        "nombre_viabilidad3": "<?php echo $razon_social['nombre_viabilidad3']; ?>",
        "numero_exterior": "<?php echo $razon_social['numero_exterior']; ?>",
        "letra_exterior": "<?php echo $razon_social['letra_exterior']; ?>",
        "edificio": "<?php echo $razon_social['edificio']; ?>",
        "edificio_piso": "<?php echo $razon_social['edificio_piso']; ?>",
        "numero_interior": "<?php echo $razon_social['numero_interior']; ?>",
        "letra_interior": "<?php echo $razon_social['letra_interior']; ?>",
        "latitud": "<?php echo $razon_social['latitud']; ?>",
        "longitud":" <?php echo $razon_social['longitud']; ?>",
        "localidad": "<?php echo $razon_social['localidad']; ?>"
        "municipio": "<?php echo $razon_social['municipio']; ?>"
        "entidad": "<?php echo $razon_social['entidad']; ?>"
<?php
if ($contador !== $ultimo) {
  $fin = '},';
  $contador += 1;
} else {
  $fin = '}';
}
?>
      <?php echo $fin; ?>

<?php } ?>
    ]
  }
}
