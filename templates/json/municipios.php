{
  "municipios": {
    "municipio": [
<?php
  $total = count($municipios);
  $ultimo = $total - 1;
  $contador = 0;
  foreach ($municipios as $municipio) {
?>
      {
        "clave_municipio": <?php echo $municipio['clave_municipio']; ?>,
        "nombre": "<?php echo $municipio['municipio']; ?>"
<?php
if ($contador !== $ultimo) {
  $fin = '},';
  $contador += 1;
} else {
  $fin = '}';
}
?>
      <?php echo $fin; ?>

<?php } ?>
    ]
  }
}
