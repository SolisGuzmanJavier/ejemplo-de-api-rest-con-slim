{
  "razones_sociales": {
    "razon_social": [
<?php
  $total = count($razones_sociales);
  $ultimo = $total - 1;
  $contador = 0;
  foreach ($razones_sociales as $razon_social) {
?>
      {
        "nombre": "<?php echo $razon_social['nombre']; ?>",
        "razon_social": "<?php echo $razon_social['razon_social']; ?>",
        "tipo_viabilidad": "<?php echo $razon_social['tipo_viabilidad']; ?>",
        "nombre_viabilidad": "<?php echo $razon_social['nombre_viabilidad']; ?>",
        "codigo_postal": "<?php echo $razon_social['codigo_postal']; ?>",
        "fecha_incorporacion": "<?php echo $razon_social['fecha_incorporacion']; ?>"
        "localidad": "<?php echo $razon_social['localidad']; ?>"
        "municipio": "<?php echo $razon_social['municipio']; ?>"
        "entidad": "<?php echo $razon_social['entidad']; ?>"
<?php
if ($contador !== $ultimo) {
  $fin = '},';
  $contador += 1;
} else {
  $fin = '}';
}
?>
      <?php echo $fin; ?>

<?php } ?>
    ]
  }
}
