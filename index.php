<?php
require 'vendor/autoload.php';
$app = new \Slim\Slim();

$app->hook('slim.before.router', function () use($app){
	
	$formato = $app->request->headers->get('Accept');
	$tipo_peticion = $app->request->getMethod();
  	
	if($tipo_peticion === 'GET' || $tipo_peticion === 'DELETE'){
	  	if ($formato !== 'application/json' && $formato !== 'application/xml') {
		$app->response->headers->set('Content-Type', 'application/json');
	    $app->halt(415,json_encode(array("error" => "Accept validos: 'application/json', 'application/xml'", "status_code"=>415)));
	  	}
  	}
  	if($tipo_peticion === 'POST' || $tipo_peticion === 'PUT'){
  	$tipo_contenido = $app->request->headers->get('Content-Type');
  		
	  	if (($tipo_contenido !== 'application/json; charset=UTF-8' && $tipo_contenido !== 'application/json')
	  	    && ($tipo_contenido !== 'application/xml; charset=UTF-8' && $tipo_contenido !== 'application/xml')) {
	    $app->response->headers->set('Content-Type', 'application/json');
	    $app->halt(415,json_encode(array("error" => "Content-Type validos: 'application/json', 'application/xml'", "status_code"=>415)));
	  	}
	}	  	

});

$db = new PDO('pgsql:dbname=bdd11190127; host=hernandezblasantonio.nsupdate.info; user=alu11190127; password=Cruzazul1$; port=1111');
//$db = new PDO('pgsql:dbname=api; host=localhost; user=postgres; password=cruzazul; port=5432');

//::::::::::::::GET:::::::::::::::::

/* GET /entidades*/
$app->get('/entidades', function() use($app, $db) {
  
  $formato = $app->request->headers->get('Accept');

  $sql_query = $db->prepare("select * from entidades");
  $sql_query->execute();
  $entidades = $sql_query->fetchAll();
  tiene_resultado($entidades, $app);  
  $app->response->headers->set('Content-Type', $formato);

  if ($formato === 'application/json') {
    $app->render('json/entidades.php', array('entidades' => $entidades));
  } else {
    $app->render('xml/entidades.php', array('entidades' => $entidades));
  }

});

/* GET /entidades/id */

$app->get('/entidades/:id', function($id) use($app, $db){
	if(!is_numeric($id)) {
	    $app->response->headers->set('Content-Type', 'text/plain');
	    $app->halt(400, "ID de Entidad no valido.");
	}	
	$formato = $app->request->headers->get('Accept');

	$sql_query = $db->prepare("select * from entidades
		where clave_entidad = :id");
	$sql_query->execute(array(':id' => $id));
	$entidades = $sql_query->fetchAll();

	tiene_resultado($entidades, $app);
    $app->response->headers->set('Content-Type', $formato);
	$entidades = quitar_espacios($entidades); 
	if ($formato === 'application/json') {
	    $app->render('json/entidades.php', array('entidades' => $entidades));
	} else {
	    $app->render('xml/entidades.php', array('entidades' => $entidades));
  	}
});
$app->get('/entidades/:id/razones_sociales', function($id) use($app, $db){
	if(!is_numeric($id)) {
	    $app->response->headers->set('Content-Type', 'text/plain');
	    $app->halt(400, "ID de Entidad no valido.");
	}	

	$formato = $app->request->headers->get('Accept');
	$sql_query = $db->prepare("
		select * from servicios_salud
		natural join clase_actividad
		where clave_entidad = :id order by id
	");
	$sql_query->execute(array(':id' => $id));
	$razones_sociales = $sql_query->fetchAll();

	tiene_resultado($razones_sociales, $app);
    $app->response->headers->set('Content-Type', $formato);
  	$razones_sociales = quitar_espacios($razones_sociales);
  
	if ($formato === 'application/json') {
	    $app->render('json/razones_sociales.php', array('razones_sociales' => $razones_sociales));
	} else {
	    $app->render('xml/razones_sociales.php', array('razones_sociales' => $razones_sociales));
  	}
  
});

/*GET entidades/:id/municipios*/
$app->get('/entidades/:id/municipios', function($id) use($app, $db){
	if(!is_numeric($id)) {
	    $app->response->headers->set('Content-Type', 'text/plain');
	    $app->halt(400, "ID de Entidad no valido.");
	}	

	$formato = $app->request->headers->get('Accept');
  
	$sql_query = $db->prepare("
		select clave_municipio, municipio
		from municipios	where clave_entidad = :id 
	");
	$sql_query->execute(array(':id' => $id));
	$municipios = $sql_query->fetchAll();

	tiene_resultado($municipios, $app);
    $app->response->headers->set('Content-Type', $formato);
  	$municipios = quitar_espacios($municipios);
	if ($formato === 'application/json') {
	    $app->render('json/municipios.php', array('municipios' => $municipios));
	}else{
		$app->render('xml/municipios.php',array('municipios' => $municipios));
	}
});

/*GET entidades/:id/municipios/id*/
$app->get('/entidades/:id/municipios/:id_municipio', function($id, $id_municipio) use($app, $db){
	if(!is_numeric($id) || !is_numeric($id_municipio)) {
	    $app->response->headers->set('Content-Type', 'text/plain');
	    $app->halt(400, "ID no valido.");
	}	

	$formato = $app->request->headers->get('Accept');
	$sql_query = $db->prepare("
		select clave_municipio, municipio
		from municipios	where clave_entidad = :id
		and clave_municipio= :id_municipio
		order by clave_municipio
	");
	$sql_query->execute(array(':id' => $id, ':id_municipio' => $id_municipio));
	$municipios = $sql_query->fetchAll();
	
	tiene_resultado($municipios, $app);
    $municipios = quitar_espacios($municipios);
    $app->response->headers->set('Content-Type', $formato);
  
	if ($formato === 'application/json') {
	    $app->render('json/municipios.php', array('municipios' => $municipios));
	}else{
		$app->render('xml/municipios.php',array('municipios' => $municipios));
	}
});
$app->get('/entidades/:id/municipios/:id_municipio/razones_sociales', function($id, $id_municipio) use($app, $db){
	if(!is_numeric($id) || !is_numeric($id_municipio)) {
	    $app->response->headers->set('Content-Type', 'text/plain');
	    $app->halt(400, "ID no valido.");
	}	

	$formato = $app->request->headers->get('Accept');
	$sql_query = $db->prepare("
		select * from servicios_salud
		natural join clase_actividad 
		where clave_entidad = :id and 
		clave_municipio= :id_municipio
		order by id
	");
	$sql_query->execute(array(':id' => $id, ':id_municipio' => $id_municipio));
	$razones_sociales = $sql_query->fetchAll();
	
	tiene_resultado($razones_sociales, $app);
    
    $app->response->headers->set('Content-Type', $formato);
  	$razones_sociales = quitar_espacios($razones_sociales);
	if ($formato === 'application/json') {
	    $app->render('json/razones_sociales.php', array('razones_sociales' => $razones_sociales));
	
	}else{
		$app->render('xml/razones_sociales.php',array('razones_sociales' => $razones_sociales));
	}
});


/*GET entidades/:id/municipios/id/localidades*/
$app->get('/entidades/:id/municipios/:id_municipio/localidades', function($id, $id_municipio) use($app, $db){
	if(!is_numeric($id) || !is_numeric($id_municipio)) {
	    $app->response->headers->set('Content-Type', 'text/plain');
	    $app->halt(400, "ID no valido.");
	}	

	$formato = $app->request->headers->get('Accept');
	$sql_query = $db->prepare("
		select clave_localidad, localidad
		from localidades where clave_entidad = :id
		and clave_municipio= :id_municipio
		order by clave_localidad 
	");
	$sql_query->execute(array(':id' => $id, ':id_municipio' => $id_municipio));
	$localidades = $sql_query->fetchAll();

	tiene_resultado($localidades, $app);

    $app->response->headers->set('Content-Type', $formato);
  	$localidades = quitar_espacios($localidades);
	if ($formato === 'application/json') {
	    $app->render('json/localidades.php', array('localidades' => $localidades));
	}else{
		$app->render('xml/localidades.php',array('localidades' => $localidades));
	}
});

$app->get('/entidades/:id/municipios/:id_municipio/localidades/:id_localidad', function($id, $id_municipio, $id_localidad) use($app, $db){
	if(!is_numeric($id) || !is_numeric($id_municipio)|| !is_numeric($id_localidad)) {
	    $app->response->headers->set('Content-Type', 'text/plain');
	    $app->halt(400, "ID no valido.");
	}	

	$formato = $app->request->headers->get('Accept');
	$sql_query = $db->prepare("
		select clave_localidad, localidad
		from localidades where clave_entidad = :id and clave_municipio= :id_municipio
		and clave_localidad= :id_localidad order by clave_localidad
	");
	$sql_query->execute(array(':id' => $id, ':id_municipio' => $id_municipio, ':id_localidad' => $id_localidad));
	$localidades = $sql_query->fetchAll();

	tiene_resultado($localidades, $app);

    $app->response->headers->set('Content-Type', $formato);
  	$localidades = quitar_espacios($localidades);
	if ($formato === 'application/json') {
	    $app->render('json/localidades.php', array('localidades' => $localidades));
	}else{
		$app->render('xml/localidades.php',array('localidades' => $localidades));
	}
});

$app->get('/entidades/:id/municipios/:id_municipio/localidades/:id_localidad/razones_sociales', function($id, $id_municipio, $id_localidad) use($app, $db){
	if(!is_numeric($id) || !is_numeric($id_municipio)|| !is_numeric($id_localidad)) {
	    $app->response->headers->set('Content-Type', 'text/plain');
	    $app->halt(400, "ID no valido.");
	}	
	$formato = $app->request->headers->get('Accept');
	$sql_query = $db->prepare("
		select * from servicios_salud 
		natural join clase_actividad where 
		clave_entidad = :id and clave_municipio= :id_municipio
		and clave_localidad= :id_localidad order by id
	");
	$sql_query->execute(array(':id' => $id, ':id_municipio' => $id_municipio, ':id_localidad' => $id_localidad));
	$razones_sociales = $sql_query->fetchAll();

	tiene_resultado($razones_sociales, $app);
    $app->response->headers->set('Content-Type', $formato);
  	$razones_sociales = quitar_espacios($razones_sociales);
	if ($formato === 'application/json') {
	    $app->render('json/razones_sociales.php', array('razones_sociales' => $razones_sociales));
	
	}else{
		$app->render('xml/razones_sociales.php',array('razones_sociales' => $razones_sociales));
	}
});

$app->get('/razones_sociales/:id_razon_social',function($id_razon_social) use($app, $db){
	
	if(!is_numeric($id_razon_social)) {
	    $app->response->headers->set('Content-Type', 'text/plain');
	    $app->halt(400, "ID no valido.");
	}	
	$formato = $app->request->headers->get('Accept');
	$sql_query = $db->prepare("
		select * from servicios_salud 
		natural join clase_actividad
		where id= :id_razon_social
	");
	$sql_query->execute(array(':id_razon_social' => $id_razon_social));
	$razones_sociales = $sql_query->fetchAll();

	tiene_resultado($razones_sociales, $app);
    $app->response->headers->set('Content-Type', $formato);
  	$razones_sociales = quitar_espacios($razones_sociales);
	if ($formato === 'application/json') {
	    $app->render('json/razones_sociales.php', array('razones_sociales' => $razones_sociales));
	
	}else{
		$app->render('xml/razones_sociales.php',array('razones_sociales' => $razones_sociales));
	}
});

$app->get('/razones_sociales/:id_razon_social/contacto',function($id_razon_social) use($app, $db){	
	if(!is_numeric($id_razon_social)) {
	    $app->response->headers->set('Content-Type', 'text/plain');
	    $app->halt(400, "ID no valido.");
	}	
	$formato = $app->request->headers->get('Accept');
	$sql_query = $db->prepare("
		select nombre, numero_telefono, correo_electronico, sitio_internet  from servicios_salud natural join clase_actividad
		where id= :id_razon_social
	");
	$sql_query->execute(array(':id_razon_social' => $id_razon_social));
	$razones_sociales = $sql_query->fetchAll();

	tiene_resultado($razones_sociales, $app);
    $app->response->headers->set('Content-Type', $formato);
  	$razones_sociales = quitar_espacios($razones_sociales);
	if ($formato === 'application/json') {
	    $app->render('json/razon_social_contacto.php', array('razones_sociales' => $razones_sociales));
	
	}else{
		$app->render('xml/razon_social_contacto.php',array('razones_sociales' => $razones_sociales));
	}
});

$app->get('/razones_sociales/:id_razon_social/direccion',function($id_razon_social) use($app, $db){	
	if(!is_numeric($id_razon_social)) {
	    $app->response->headers->set('Content-Type', 'text/plain');
	    $app->halt(400, "ID no valido.");
	}	
	$formato = $app->request->headers->get('Accept');
	$sql_query = $db->prepare("
		SELECT nombre, tipo_viabilidad, nombre_viabilidad, tipo_viabilidad1, nombre_viabilidad1, 
		tipo_viabilidad2, nombre_viabilidad2, tipo_viabilidad3, nombre_viabilidad3, numero_exterior, letra_exterior, edificio, 

		edificio_piso, numero_interior, letra_interior, latitud, longitud, localidad, municipio, entidad	
		from servicios_salud natural join localidades natural join municipios natural join entidades
		where id= :id_razon_social
	");
	$sql_query->execute(array(':id_razon_social' => $id_razon_social));
	$razones_sociales = $sql_query->fetchAll();

	tiene_resultado($razones_sociales, $app);
    $app->response->headers->set('Content-Type', $formato);
  	$razones_sociales = quitar_espacios($razones_sociales);
	if ($formato === 'application/json') {
	    $app->render('json/razon_social_direccion.php', array('razones_sociales' => $razones_sociales));	
	}else{
		$app->render('xml/razon_social_direccion.php',array('razones_sociales' => $razones_sociales));
	}
});


$app->get('/razones_sociales/:id_razon_social/datos_generales',function($id_razon_social) use($app, $db){	
	if(!is_numeric($id_razon_social)) {
	    $app->response->headers->set('Content-Type', 'text/plain');
	    $app->halt(400, "ID no valido.");
	}	
	$formato = $app->request->headers->get('Accept');
	$sql_query = $db->prepare("
		SELECT nombre, razon_social, tipo_viabilidad, nombre_viabilidad, codigo_postal, fecha_incorporacion, localidad, municipio, entidad
		from servicios_salud natural join localidades natural join municipios natural join entidades
		where id= :id_razon_social
	");
	$sql_query->execute(array(':id_razon_social' => $id_razon_social));
	$razones_sociales = $sql_query->fetchAll();

	tiene_resultado($razones_sociales, $app);
    $app->response->headers->set('Content-Type', $formato);
  	$razones_sociales = quitar_espacios($razones_sociales);
	if ($formato === 'application/json') {
	    $app->render('json/razon_social_generales.php', array('razones_sociales' => $razones_sociales));	
	}else{
		$app->render('xml/razon_social_generales.php',array('razones_sociales' => $razones_sociales));
	}
});
$app->get('/clase_actividad',function() use($app, $db){	
	$formato = $app->request->headers->get('Accept');
	$sql_query = $db->prepare("
		SELECT *
		from clase_actividad
	");
	$sql_query->execute();
	$clase_actividad = $sql_query->fetchAll();

	tiene_resultado($clase_actividad, $app);
    $app->response->headers->set('Content-Type', $formato);
  	$clase_actividad = quitar_espacios($clase_actividad);
	if ($formato === 'application/json') {
	    $app->render('json/clase_actividad.php', array('clase_actividad' => $clase_actividad));	
	}else{
		$app->render('xml/clase_actividad.php',array('clase_actividad' => $clase_actividad));
	}
});
$app->get('/clase_actividad/:id/razones_sociales',function($id) use($app, $db){	
	$formato = $app->request->headers->get('Accept');
	$sql_query = $db->prepare("
		SELECT *
		from clase_actividad 
		natural join servicios_salud
		where codigo_de_actividad =:id
	");
	$sql_query->execute(array(':id' => $id));
	//print_r($sql_query->errorInfo());
	$razones_sociales = $sql_query->fetchAll();

	tiene_resultado($razones_sociales, $app);
    $app->response->headers->set('Content-Type', $formato);
  	$razones_sociales = quitar_espacios($razones_sociales);
	if ($formato === 'application/json') {
	    $app->render('json/razones_sociales.php', array('razones_sociales' => $razones_sociales));	
	}else{
		$app->render('xml/razones_sociales.php',array('razones_sociales' => $razones_sociales));
	}

});


$app->get('/estadisticas/entidades', function() use($app, $db){
	
	$formato = $app->request->headers->get('Accept');

	$sql_query = $db->prepare('
		SELECT clave_entidad, entidad,
		"count"(id)  razones_sociales FROM servicios_salud 
		natural join entidades
		GROUP BY clave_entidad, entidad ORDER BY clave_entidad
	');
	$sql_query->execute();
	$estadisticas = $sql_query->fetchAll();
	//print_r($sql_query->errorInfo());
	tiene_resultado($estadisticas, $app);
    $app->response->headers->set('Content-Type', $formato);
	$estadisticas = quitar_espacios($estadisticas); 

	$sql_query = $db->prepare('
		SELECT 	clave_entidad, "count"(clave_municipio) municipios
		FROM municipios
		GROUP BY clave_entidad ORDER BY clave_entidad
	');
	$sql_query->execute();
	$municipios = $sql_query->fetchAll();
	tiene_resultado($municipios, $app);
    $municipios = quitar_espacios($municipios); 

	$sql_query = $db->prepare('
		SELECT 	clave_entidad, "count"(clave_localidad) localidades
		FROM localidades
		GROUP BY clave_entidad ORDER BY clave_entidad
	');
	$sql_query->execute();
	$localidades = $sql_query->fetchAll();
	tiene_resultado($localidades, $app);
    $localidades = quitar_espacios($localidades); 
	
	foreach ($estadisticas as $key => $value) {
		$estadisticas[$key]['municipios'] = $municipios[$key]['municipios'];
		$estadisticas[$key]['localidades'] = $localidades[$key]['localidades'];
	}
	
	if ($formato === 'application/json') {
	    $app->render('json/estadisticas_entidades.php', array('estadisticas' => $estadisticas));
	} else {
	    $app->render('xml/estadisticas_entidades.php', array('estadisticas' => $estadisticas));
  	}
});

$app->get('/estadisticas/entidades/:id', function($id) use($app, $db){
	if(!is_numeric($id)) {
	    $app->response->headers->set('Content-Type', 'text/plain');
	    $app->halt(400, "ID no valido.");
	}	
	
	$formato = $app->request->headers->get('Accept');

	$sql_query = $db->prepare('
		SELECT clave_entidad, entidad,
		"count"(id)  razones_sociales FROM servicios_salud 
		natural join entidades where clave_entidad =:id
		GROUP BY clave_entidad, entidad ORDER BY clave_entidad
	');
	$sql_query->execute(array(':id' => $id ));
	$estadisticas = $sql_query->fetchAll();
	tiene_resultado($estadisticas, $app);
    $app->response->headers->set('Content-Type', $formato);
	$estadisticas = quitar_espacios($estadisticas); 

	$sql_query = $db->prepare('
		SELECT 	clave_entidad, "count"(clave_municipio) municipios
		FROM municipios where clave_entidad =:id
		GROUP BY clave_entidad ORDER BY clave_entidad
	');
	$sql_query->execute(array(':id' => $id ));
	$municipios = $sql_query->fetchAll();
	tiene_resultado($municipios, $app);
    $municipios = quitar_espacios($municipios); 

	$sql_query = $db->prepare('
		SELECT 	clave_entidad, "count"(clave_localidad) localidades
		FROM localidades where clave_entidad =:id
		GROUP BY clave_entidad ORDER BY clave_entidad
	');
	$sql_query->execute(array(':id' => $id ));
	$localidades = $sql_query->fetchAll();
	tiene_resultado($localidades, $app);
    $localidades = quitar_espacios($localidades); 
	
	foreach ($estadisticas as $key => $value) {
		$estadisticas[$key]['municipios'] = $municipios[$key]['municipios'];
		$estadisticas[$key]['localidades'] = $localidades[$key]['localidades'];
	}
	
	if ($formato === 'application/json') {
	    $app->render('json/estadisticas_entidades.php', array('estadisticas' => $estadisticas));
	} else {
	    $app->render('xml/estadisticas_entidades.php', array('estadisticas' => $estadisticas));
  	}
});
// ::::::::POST:::::::::::

$app->post('/entidades/:id/municipios', function($id) use($app, $db){
	if(!is_numeric($id)) {
		$app->response->headers->set('Content-Type', 'application/json');
		$app->halt(400,json_encode(array("mensaje" => "ID no valido", "status_code"=>400)));
	}	
	$datos = arreglo_de_datos($app,'municipios', 'municipio');
	$schema = array("nombre" => ['requerido','letras']); 
	schema_correcto($app, $schema, $datos);

	$nombre_nuevo = $datos['nombre'];
	$sql_query = $db->prepare("
		select max(clave_municipio)
		from municipios
		where clave_entidad = :id " 
	);
	$sql_query->execute(array(':id' => $id));
	$id_municipio = $sql_query->fetchAll();
	$id_municipio = $id_municipio[0]['max']+1;
	$sql_query = $db->prepare("
		insert into
    	municipios (clave_municipio, municipio, clave_entidad)
    	values (:id_municipio, :nombre_nuevo,:id)
	");
	$sql_query->execute(array(':id_municipio'=> $id_municipio, 'nombre_nuevo'=> $nombre_nuevo, ':id' => $id));
	
	if ($sql_query->rowCount() === 0) {
	    $app->response->headers->set('Content-Type', 'application/json');
	    $app->halt(415,json_encode(array("error" => "error al crear el Municipio", "status_code"=>415)));
	}
  	$app->response->headers->set('Content-Type', 'application/json');
  	$app->halt(201,json_encode(array("mensaje" => "El Municipio ha sido creado", "status_code"=>201, "id_municipio"=>$id_municipio)));
	
});

$app->post('/entidades/:id/municipios/:id_municipio/localidades', function($id, $id_municipio) use($app, $db){
	if(!is_numeric($id) || !is_numeric($id_municipio)) {
	    $app->response->headers->set('Content-Type', 'application/json');
		$app->halt(400,json_encode(array("mensaje" => "ID no valido", "status_code"=>400)));
	}	

	$datos = arreglo_de_datos($app,'localidades', 'localidad');
	$schema = array("nombre" => ['requerido','letras']); 
	schema_correcto($app, $schema, $datos);
	
	$nombre_nuevo = $datos['nombre'];
	$sql_query = $db->prepare("
		select max(clave_localidad)
		from localidades
		where clave_entidad = :id and 
		clave_municipio = :id_municipio" 
	);

	$sql_query->execute(array(':id' => $id, 'id_municipio'=>$id_municipio));
	$id_localidad = $sql_query->fetchAll();
	$id_localidad = $id_localidad[0]['max']+1;
	$sql_query = $db->prepare("
		insert into localidades
		(clave_localidad, localidad, clave_municipio, clave_entidad)
    	values (:id_localidad,:nombre_nuevo,:id_municipio,:id)
	");
	$sql_query->execute(array('id_localidad'=>$id_localidad,'nombre_nuevo'=> $nombre_nuevo,':id_municipio'=> $id_municipio, ':id' => $id));
	
	if ($sql_query->rowCount() === 0) {
	    $app->response->headers->set('Content-Type', 'application/json');
	    $app->halt(415,json_encode(array("error" => "error al crear la localidad", "status_code"=>415)));
	}
	$app->response->headers->set('Content-Type', 'application/json');
	$app->halt(201,json_encode(array("mensaje" => "La localidad ha sido creada", "status_code"=>201,"id_localidad"=>$id_localidad)));
	
});

$app->post('/entidades/:id/municipios/:id_municipio/localidades/:id_localidad/razones_sociales', function($id, $id_municipio, $id_localidad) use($app, $db){
	if(!is_numeric($id) || !is_numeric($id_municipio)||!is_numeric($id_municipio)) {
    	$app->response->headers->set('Content-Type', 'application/json');
		$app->halt(400,json_encode(array("mensaje" => "ID no valido", "status_code"=>400)));
	}	

	$datos = arreglo_de_datos($app,'razones_sociales', 'razon_social');
	
	$schema = array("nombre" => ['requerido'] ,"razon_social" => ['requerido'],"codigo_de_actividad" => ['requerido','numerico'],
					"descripcion_estrato_personal" => ['requerido'],"tipo_viabilidad" => ['requerido'],"nombre_viabilidad" => ['requerido'],"tipo_viabilidad1" => [],
					"nombre_viabilidad1" => [],"tipo_viabilidad2" => [],"nombre_viabilidad2" => [],"tipo_viabilidad3" => [],"nombre_viabilidad3" => [],
                    "numero_exterior" => ['numerico'],"letra_exterior" => [],"edificio" => [],"edificio_piso" => [],"numero_interior" => [],
                    "letra_interior" => [],"tipo_asentamiento_humano" => ['letras'],"nombre_asentamiento_humano" => ['letras'],"tipo_centro_comercial" => [],
                    "corredor_industrial_centro_comercial" => [],"numero_local" => [],"codigo_postal" => ['numerico'],"area_geoestadistica_basica" => [],
                    "manzana" => ['numerico'],"numero_telefono" => ['numerico',10],"correo_electronico" => ['email'],"sitio_internet" => [],
                    "tipo_establecimiento" => [],"latitud" => ['numerico'],"longitud" => ['numerico'],"fecha_incorporacion" => []
	); 
	schema_correcto($app, $schema, $datos);

	$sql_query = $db->prepare("
		select max(id) from  servicios_salud
	");
	$sql_query->execute();
	$id_razon_social = $sql_query->fetchAll();
	
	$id_razon_social = $id_razon_social[0]['max']+1;

	/*Datos de razones_sociales*/
	$sql_query = $db->prepare("
		insert into servicios_salud 
    	 values(:id,:nombre, :razon_social, :codigo_de_actividad, :descripcion_estrato_personal,
			:tipo_viabilidad, :nombre_viabilidad,:tipo_viabilidad1, :nombre_viabilidad1,
			:tipo_viabilidad2, :nombre_viabilidad2,:tipo_viabilidad3, :nombre_viabilidad3,
			:numero_exterior, :letra_exterior, :edificio, :edificio_piso, :numero_interior,
			:letra_interior, :tipo_asentamiento_humano, :nombre_asentamiento_humano,
			:tipo_centro_comercial, :corredor_industrial_centro_comercial, :numero_local,
			:codigo_postal,:clave_localidad,:area_geoestadistica_basica,:manzana,:numero_telefono,
			:correo_electronico, :sitio_internet,:tipo_establecimiento,:latitud,:longitud,
			:fecha_incorporacion,:clave_municipio, :clave_entidad
	)");
	$datos['id'] = $id_razon_social;
	$datos['clave_entidad'] = $id;
	$datos['clave_municipio'] = $id_municipio;
	$datos['clave_localidad'] = $id_localidad;

	$sql_query->execute($datos);

	if ($sql_query->rowCount() === 0) {
	    $app->response->headers->set('Content-Type', 'application/json');
		$app->halt(415,json_encode(array("error" => "no se pudo crear la razon social", "status_code"=>415)));
  	}
	
	$app->response->headers->set('Content-Type', 'application/json');
	$app->halt(201,json_encode(array("mensaje" => "La razon_social ha sido creada", "status_code"=>201,"id_razon_social"=>$id_razon_social)));
	


});
//::::::::PUT:::::::::::
$app->put('/razones_sociales/:id_razon_social', function($id_razon_social) use($app, $db) {

	$sql_query = $db->prepare("
	    select id from servicios_salud
	    where id = :id_razon_social
	");
	$sql_query->execute(array(':id_razon_social' => $id_razon_social));
	
	$razon_social = $sql_query->fetchAll();
	
	tiene_resultado($razon_social, $app);
	
	$datos = arreglo_de_datos($app, 'razones_sociales', 'razon_social');

	$schema = array("nombre" => ['requerido'] ,"razon_social" => [],"codigo_de_actividad" => ['requerido','numerico'],
					"descripcion_estrato_personal" => [],"tipo_viabilidad" => [],"nombre_viabilidad" => [],"tipo_viabilidad1" => [],
					"nombre_viabilidad1" => [],"tipo_viabilidad2" => [],"nombre_viabilidad2" => [],"tipo_viabilidad3" => [],"nombre_viabilidad3" => [],
                    "numero_exterior" => ['numerico'],"letra_exterior" => [],"edificio" => [],"edificio_piso" => [],"numero_interior" => [],
                    "letra_interior" => [],"tipo_asentamiento_humano" => ['letras'],"nombre_asentamiento_humano" => ['letras'],"tipo_centro_comercial" => [],
                    "corredor_industrial_centro_comercial" => [],"numero_local" => [],"codigo_postal" => ['numerico'],"area_geoestadistica_basica" => [],
                    "manzana" => ['numerico'],"numero_telefono" => ['numerico',10],"correo_electronico" => ['email'],"sitio_internet" => [],
                    "tipo_establecimiento" => ['letras'],"latitud" => [],"longitud" => [],"fecha_incorporacion" => []
	); 
	
	schema_correcto($app, $schema, $datos);
	
	$sql_query = $db->prepare("
	    update servicios_salud
	    set nombre = :nombre, razon_social = :razon_social, codigo_de_actividad = :codigo_de_actividad, 
	    descripcion_estrato_personal = :descripcion_estrato_personal, tipo_viabilidad = :tipo_viabilidad,
	    nombre_viabilidad = :nombre_viabilidad, tipo_viabilidad1 = :tipo_viabilidad1, nombre_viabilidad1 =:nombre_viabilidad1,
		tipo_viabilidad2 = :tipo_viabilidad2, nombre_viabilidad2 = :nombre_viabilidad2,tipo_viabilidad3 = :tipo_viabilidad3,
		nombre_viabilidad3 =:nombre_viabilidad3,numero_exterior= :numero_exterior,letra_exterior= :letra_exterior,
		edificio =:edificio,edificio_piso =:edificio_piso,numero_interior =:numero_interior,letra_interior =:letra_interior,
		tipo_asentamiento_humano =:tipo_asentamiento_humano,nombre_asentamiento_humano=:nombre_asentamiento_humano,
		tipo_centro_comercial=:tipo_centro_comercial,corredor_industrial_centro_comercial =:corredor_industrial_centro_comercial, 
		numero_local =:numero_local,codigo_postal=:codigo_postal,area_geoestadistica_basica=:area_geoestadistica_basica,
		manzana=:manzana,numero_telefono= :numero_telefono,correo_electronico =:correo_electronico,sitio_internet =:sitio_internet,
		tipo_establecimiento =:tipo_establecimiento,latitud =:latitud, longitud= :longitud,fecha_incorporacion =:fecha_incorporacion
	    where id = :id_razon_social
	");
	$datos['id_razon_social'] = $id_razon_social;
	$sql_query->execute($datos);

	if ($sql_query->rowCount() === 0) {
   	    $app->response->headers->set('Content-Type', 'application/json');
		$app->halt(415,json_encode(array("error" => "no se pudo actualizar", "status_code"=>415)));

  	}
    $app->response->headers->set('Content-Type', 'application/json');
	$app->halt(201,json_encode(array("mensaje" => "La razon social ha sido modificada", "status_code"=>415)));

});
$app->put('/razones_sociales/:id_razon_social/contacto', function($id_razon_social) use($app, $db) {

	$sql_query = $db->prepare("
	    select id from servicios_salud
	    where id = :id_razon_social
	");
	$sql_query->execute(array(':id_razon_social' => $id_razon_social));
	
	$razon_social = $sql_query->fetchAll();
	
	tiene_resultado($razon_social, $app);
	
	$datos = arreglo_de_datos($app, 'razones_sociales', 'razon_social');

	$schema = array("nombre" => ['requerido'],"numero_telefono" => ['numerico',10],
			"correo_electronico" => ['email'],"sitio_internet" => []            
	); 
	
	schema_correcto($app, $schema, $datos);
	
	$sql_query = $db->prepare("
	    update servicios_salud
	    set nombre = :nombre, numero_telefono= :numero_telefono,
	    correo_electronico =:correo_electronico,sitio_internet =:sitio_internet
	    where id = :id_razon_social
	");
	$datos['id_razon_social'] = $id_razon_social;
	$sql_query->execute($datos);

	if ($sql_query->rowCount() === 0) {
   	    $app->response->headers->set('Content-Type', 'application/json');
		$app->halt(415,json_encode(array("error" => "no se pudo actualizar", "status_code"=>415)));

  	}
    $app->response->headers->set('Content-Type', 'application/json');
	$app->halt(201,json_encode(array("mensaje" => "La razon social ha sido modificada", "status_code"=>415)));	
});

	//::::::::DELETE:::::::::::

	$app->delete('/razones_sociales/:id_razon_social',function($id_razon_social) use($app, $db) {
	  if(!is_numeric($id_razon_social)) {
	    $app->response->headers->set('Content-Type', 'text/plain');
	    $app->halt(400, "ID's no validos.");
	  }
	  
	  $sql_query = $db->prepare("
	    delete from servicios_salud
	    where id = :id_razon_social
	  ");
	  $sql_query->execute(array(':id_razon_social' => $id_razon_social));

    if ($sql_query->rowCount() === 0) {
   	    $app->response->headers->set('Content-Type', 'application/json');
		$app->halt(404,json_encode(array("error" => "Recurso no encontrado", "status_code"=>404)));
    }
    $app->response->headers->set('Content-Type', 'application/json');
	$app->halt(200,json_encode(array("mensaje" => "La razon social ha sido borrada", "status_code"=>200)));

});

/*funcion para validar el esquema del arreglo de datos */
function schema_correcto($app,$schema, $datos){
	$indices_schema = array_keys($schema);
	$indices_datos = array_keys($datos);
  	$tipo_contenido = $app->request->headers->get('Content-Type');
  	
	if($indices_schema !== $indices_datos){
		$app->response->headers->set('Content-Type', 'text/plain');
	    $app->halt(415, $tipo_contenido." con campos incorrectos");
	}
	foreach ($schema as $key => $value) {

		for ($i=0; $i < count($value); $i++) { 

			if ($value[$i]==='requerido') {
				if(empty($datos[$key])){
					$app->response->headers->set('Content-Type', 'application/json');
	      			$app->halt(415,json_encode(array("error" => "el campo ".$key." esta  vacio ", "status_code"=>415)));
				}
			}
			if ((strlen($datos[$key]) >0 )) {
				if ($value[$i]==='letras'){
					if(!preg_match("/^[a-z ]+$/i", $datos[$key])){
						$app->response->headers->set('Content-Type', 'application/json');
	      				$app->halt(415,json_encode(array("error" => "el campo ".$key." necesita contener solo letras", "status_code"=>415)));			
					}
				}
				if ($value[$i]==='numerico') {
					if(!is_numeric($datos[$key])){
						$app->response->headers->set('Content-Type', 'application/json');
	      				$app->halt(415,json_encode(array("error" => "el campo ".$key." solo acepta numeros ", "status_code"=>415)));
					}
				}
				if ($value[$i]==='email') {
					if(! filter_var($datos[$key], FILTER_VALIDATE_EMAIL)){
					$app->response->headers->set('Content-Type', 'application/json');
	      			$app->halt(415,json_encode(array("error" => "el campo ".$key." necesita formato de email ", "status_code"=>415)));
					}		
				}
				if (is_numeric($value[$i])) {
					if(strlen(trim($datos[$key]))!== $value[$i]){
						$app->response->headers->set('Content-Type', 'application/json');
	      				$app->halt(415,json_encode(array("error" => "el campo ".$key." necesita tener ".$value[$i]." caracteres", "status_code"=>415)));
					}		
				}	
			}
		}
	}
}
/*funcion que nos devuelve el cuerpo del peticion en forma de arreglo*/
function arreglo_de_datos($app, $coleccion, $individuo){

	$tipo_contenido = $app->request->headers->get('Content-Type');
	$cuerpo_solicitud = $app->request->getBody();
	if ($tipo_contenido === 'application/json; charset=UTF-8'||$tipo_contenido === 'application/json') {
	    /* Procesar JSON */
	    $datos =  @json_decode($cuerpo_solicitud)->$coleccion->$individuo;
	    if (is_null($datos) || json_last_error() !== JSON_ERROR_NONE || count($datos) !== 1) {
	      $app->response->headers->set('Content-Type', 'application/json');
	      $app->halt(415,json_encode(array("error" => "json no valido", "status_code"=>415)));
	      
	    }
	
	}  
	if ($tipo_contenido === 'application/xml; charset=UTF-8' ||$tipo_contenido === 'application/xml') {
	    /* Procesar XML */
	    $datos = @simplexml_load_string($cuerpo_solicitud)->$individuo;
	    if (empty($datos) || count($datos) !== 1) {
	      $app->response->headers->set('Content-Type', 'application/json');
	      $app->halt(415,json_encode(array("error" => "xml no valido", "status_code"=>415)));
	    }
	}
	$datos = objectToArray($datos[0]);
	foreach ($datos as $key => $value) {
		if(is_array($datos[$key])){
			$datos[$key]= " ";			
		}
		$datos[$key] = strtoupper(trim($datos[$key]));
		if($datos[$key] === ""){
			$datos[$key] = null;
		}
	}

	return $datos;
}  
/*funcion que pasa el objeto  a un arreglo*/
function objectToArray($d) {
	 if (is_object($d)) {
	 	$d = get_object_vars($d);
	 }
	 if (is_array($d)) {
	 	return array_map(__FUNCTION__, $d);
	 }
	 else {
	 return $d;
	 }
}
/*function para saber si que un produjo un resultado*/
function tiene_resultado($query, $app) {
	if (count($query) === 0) {
	    $app->response->headers->set('Content-Type', 'application/json');
	    $app->halt(404, json_encode(array("error" => "Recurso no encontrado", "status_code"=>404)));
 	}
}
function quitar_espacios($datos){
	foreach ($datos as $key => $value) {
		foreach ($value as $key2 => $value2) {
			$datos[$key][$key2] = strtoupper(trim(trim($value2, '"')));		
		}
	}
	return $datos;
}

$app->run();
